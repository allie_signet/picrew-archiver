import subprocess
import json
import sys

picrews = []

with open("picrews.json","r+") as f:
    picrews = json.load(f)
    l = len(picrews)
    i = 0
    for p in picrews:
        if not p.get('done',False):
            link = 'https://picrew.me' + p['link']
            print(f'{i}/{l} - {p["link"]}')
            p = subprocess.run(["node","index.js",link])

            picrews[i]['done'] = True
            f.seek(0)
            f.write(json.dumps(picrews,ensure_ascii=False))
            f.truncate()
        i += 1
