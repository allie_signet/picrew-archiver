const puppeteer = require("puppeteer")
const pretty = require("beautify.log").default
const readline = require("readline")
const fs = require("fs")
const { PuppeteerBlocker } = require("@cliqz/adblocker-puppeteer")
const tmp = require('tmp')

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

const tmp_user_data = tmp.dirSync()

puppeteer.launch({
    headless: true,
    args: ["--proxy-server=127.0.0.1:8000", "--disable-web-security", "--disk-cache-dir=/dev/null", "--ignore-certificate-errors", `--user-data-dir=${tmp_user_data.name}`, "--disable-application-cache"]
}).then(async (browser) => {

    const page = await browser.newPage()

    let blocker = await PuppeteerBlocker.parse(fs.readFileSync("easylist.txt", "utf-8")) // set up ad-blocking
    blocker.enableBlockingInPage(page)

    const userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0" // trickery,just to be sure
    await page.setUserAgent(userAgent)

    await page.goto(process.argv[2])

    async function wait_and_click(selector) { // convenience function to wait for an element to exist, then click it.
        let e = await page.waitForSelector(selector, {
            visible: true
        })
        await e.click()
    }

    async function find_all_li(selector) {
        let ul = await page.waitForSelector(selector) // convenience function to find all li children of a ul
        return (await ul.$$("li"))
    }

    await wait_and_click(".main_terms_agree_btn") // picrew terms confirmation
    await wait_and_click(".imagemaker_info_btn_start") // picrew image maker start

    let categories_bar = await find_all_li(".imagemaker_parts_menu > ul")
    let categories = await page.$$(".splide__slide")

    let palette_button = await page.waitForSelector(".btn_show_colorpalette")
    let asset_button = await page.waitForSelector(".btn_show_itemlist")

    pretty.log(`{fgBlue}found ${categories_bar.length} categories`)
    let i = 0

    for (let category_icon of categories_bar) {
        await category_icon.click() // choose category

        await sleep(1000) // make sure stuff's loaded

        let category = categories[i]

        pretty.log(`{fgGreen}scraping category ${i+1}/${categories_bar.length}`)

        let assets = await category.$$(".imagemaker_itemlist > ul > li") // find category's assets
        let colors = await category.$$(".imagemaker_colorpalette > ul > li") // find category's color choices

        let all_combinations = colors.length > 0 ? assets.length * colors.length : assets.length // find total possible combinations of assets and colors
        pretty.log(`{fgCyan}found up to ${all_combinations} combinations`)

        let c_i = 0

        for (let asset of assets) {
            await sleep(200) // sleep out of politeness
            await asset_button.click() // switch to asset view

            await sleep(800) // just to make sure everything is loaded in
            await asset.click() // choose asset

            let has_colors = await page.$eval(".btn_show_colorpalette", (node) => {
                return !(node.classList.contains("disabled") || node.classList.contains("none"))
            }) // if this returns false, the palette button is disabled -> this asset has no color choices.

            if (has_colors && colors.length > 0) {
                await palette_button.click() // switch to palette view
                await sleep(500)

                for (let color of colors) {
                    try {
                        await color.click() // choose color
                    } catch (e) {
                        pretty.log(`{fgRed}[ERR} ${e}`) // this is mostly to account for palette button behaviour i forgot to account for.
                    }

                    await sleep(400) // sleep out of politeness for the servers

                    c_i += 1

                    readline.clearLine(process.stdout)
                    readline.cursorTo(process.stdout, 0)
                    process.stdout.write(`${c_i}/${all_combinations} done`) // print out a nice progress thingy
                }
            } else {
                if (colors.length > 0) {
                    all_combinations -= colors.length // this specific asset has no color choices, so reduce total combinations number by the number of colors other assets can have.
                }
                process.stdout.write(`${c_i}/${all_combinations} done`)
            }
        }
        process.stdout.write("\n")
        i += 1
    }

    await page.close()
    await browser.close()
    return
})

tmp_user_data.removeCallback()
