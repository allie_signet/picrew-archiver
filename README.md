# picrew archiver
a small tool to archive picrews.

## usage
`node index.js <picrew url>`

## notes
- this tool expects a proxy on 127.0.0.1:8000, which is responsible for actually saving the assets. i recommend [warcprox](https://github.com/internetarchive/warcprox)
- turning off headless mode in index.js and watching it run is very fun
- this tool is for preservation, not stealing art.
