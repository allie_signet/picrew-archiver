from bs4 import BeautifulSoup
import requests
import time
import json

all_picrews = []

proxies = {"http":"http://127.0.0.1:8000","https":"http://127.0.0.1:8000"}

def do_page(text):
    picrews = []
    soup = BeautifulSoup(text)
    for item in soup.find_all(class_="search-ImagemakerList_Result"):
        picrew = {
            'link': item.a['href'],
            'icon': item.find(class_="search-ImagemakerList_Icon").get_text().strip(),
            'type': item.find(class_="search-ImagemakerList_Type").get_text().strip(),
            'title': item.find(class_="search-ImagemakerList_Title").get_text().strip(),
            'creator': item.find(class_="search-ImagemakerList_Creator").get_text().strip()
        }

        picrew['permissions'] = list(map(lambda x: ('is_can' in x.get('class',[]),x.div.get_text().strip()),
        item.find(class_="search-ImagemakerList_UseRange").find_all("li")))

        picrews.append(picrew)

    return picrews

for i in range(1,467):
    r = requests.get("https://picrew.me/search",params={"s": 1,"page":i},proxies=proxies,verify=False)
    all_picrews += do_page(r.text)
    print(f"{i}/466")
    time.sleep(1)

with open("res.json","w") as f:
    f.write(json.dumps(all_picrews))
